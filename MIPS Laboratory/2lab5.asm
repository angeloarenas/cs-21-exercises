.text
main:
	li $s0, 0xfead
	li $s1, 0x1234
	li $s2, 0x7421
	
	li $a0, 1
	li $a1, 2
	li $a2, 3
	li $a3, 4

	#Jump with arguments
	jal proc
	
	move $a0, $v0
	li $v0, 1
	syscall
	
exit:
	li $v0, 10
	syscall 
	
proc:
	#proc (a,b ,c,d) returns (a+b)-(c+d)
	#push saved registers to stack
	#subtract 3 register spaces (4*3)bytes
	subi $sp, $sp, 12
	sw $s0, 8($sp)
	sw $s1, 4($sp)
	sw $s2, 0($sp)
	
	# $s0 = a, $s1 = b
	add $s0, $a0, $a1
	add $s1, $a2, $a3
	sub $s2, $t0, $t1
	#Store to v0 the return value
	move $v0, $s2
	
	#Pop saved registers from stack
	lw $s2, 0($sp)
	lw $s1, 4($sp)
	lw $s0, 8($sp)
	addi $sp, $sp, 12
	
	#return address of next instruction 
	jr $ra