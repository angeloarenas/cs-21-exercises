#if else
.data 
	numstr: .asciiz "1234"
	eq: .asciiz "Equal\n"
	neq: .asciiz "Not Equal\n"
.text
main:
	li $t0, 1
	bne $t0, 1, not_equal
	
	equal:
		la $a0, eq
		li $v0, 4
		syscall
		j exit
	
	not_equal:
		la $a0, neq
		li $v0, 4
		syscall
	
	exit:	
	li $v0, 10
	syscall