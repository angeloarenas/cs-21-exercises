.data
.text
	# Get first input
	li $v0, 5
	syscall
	move $s0, $v0
	
	# Get second input
	li $v0, 5
	syscall
	move $s1, $v0
	
	# Add store to $s2
	add $s2, $s0, $s1
	
	# Print
	move $a0, $s2
	li $v0, 1
	syscall
	
	# exit
	li $v0, 10
	syscall