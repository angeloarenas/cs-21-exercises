.text
main:
	li $a0, 1
	li $a1, 2
	li $a2, 3
	li $a3, 4

	#Jump with arguments
	jal proc
	
	move $a0, $v0
	li $v0, 1
	syscall
	
exit:
	li $v0, 10
	syscall 
	
proc:
	#proc (a,b ,c,d) returns (a+b)-(c+d)
	# $a0 = a, $a1 = b
	add $t0, $a0, $a1
	add $t1, $a2, $a3
	sub $t2, $t0, $t1
	#Store to v0 the return value
	move $v0, $t2
	#return address of next instruction 
	jr $ra