.data
	overflow: .asciiz "overflow"
.text
main:
	#Get integer
	li $v0, 5
	syscall
	move $t0, $v0
	
	#Store to $t3 the highest possible input
	li $t3, 1
	sll $t3, $t3, 16
	bge $t0, $t3, print_overflow
	
	#multiply by 2^15
	sll $t1, $t0, 15
		
	#t2 contains leftmost bit of $t1
	move $t2, $t1
	srl $t2, $t2, 31
	
	beq $t2, 1, print_overflow
	
print_answer:
	move $a0, $t1
	li $v0, 1
	syscall
	j exit
	
print_overflow:
	la $a0, overflow
	li $v0, 4
	syscall
exit:	
	li $v0, 10
	syscall