#If else
.data 
	o: .asciiz "odd\n"
	e: .asciiz "even\n"
.text
main:
	#Get input save to $t0
	li $v0, 5
	syscall
	move $t0, $v0
	#get last bit
	andi $t0, $t0, 0x00000001
	
	beq $t0, 1, print_odd
	
	print_even:
		la $a0, e
		li $v0, 4
		syscall
		j exit
		
	print_odd:
		la $a0, o
		li $v0, 4
		syscall			
	
	exit:	
		li $v0, 10
		syscall