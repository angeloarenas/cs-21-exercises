#for loop
.data 
	input: .asciiz "Input: "
	nl: .asciiz "\n"
.text
main:
	la $a0, input
	li $v0,	4 
	syscall
	
	#Get input save to $t0
	li $v0, 5
	syscall
	move $t0, $v0
	
	loop:
		blt $t0, 1, exit
		move $a0, $t0
		li $v0, 1
		syscall
		
		la $a0, nl
		li $v0, 4
		syscall
		
		subi $t0, $t0, 1
		j loop
		
	exit:	
		li $v0, 10
		syscall