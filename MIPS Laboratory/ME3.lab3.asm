.data
	upstring: .space 4
	downstring: .space 4
.text
	# Get string
	la $a0, upstring
	li $a1, 5
	li $v0, 8
	syscall
	
	# Store upstring to $t0
	lw $t0, upstring
	
	# Convert to Uppercase store to downstring
	addi $t0, $t0, 0x20202020
	sw $t0, downstring
	
	# Print upstring
	la $a0, downstring
	li $v0, 4
	syscall
	
	# Exit
	li $v0, 10
	syscall