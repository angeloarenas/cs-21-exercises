#do While loop
.data 

.text
main:
	loop:
	#Get input save to $t0
	li $v0, 5
	syscall
	move $t0, $v0
	#get last bit
	andi $t1, $t0, 0x00000001
	
	beq $t1, 1, loop
	
	exit:	
		li $v0, 10
		syscall