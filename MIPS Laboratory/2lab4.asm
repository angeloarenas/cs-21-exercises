#Reverse string
.data 
	numstr: .asciiz "1234"

.text
main:
	#mask 0x34 33 32 31
	lw $t5, numstr
	andi $t6, $t5, 0x000000ff
	andi $t7, $t5, 0x0000ff00
	andi $t8, $t5, 0x00ff0000
	andi $t9, $t5, 0xff000000
		
	#shift
	ror $t1, $t6, 8
	sll $t2, $t7, 8
	srl $t3, $t8, 8
	rol $t4, $t9, 8
	
	#set
	or $t0, $t0, $t1
	or $t0, $t0, $t2
	or $t0, $t0, $t3
	or $t0, $t0, $t4
	
	li $v0, 10
	syscall