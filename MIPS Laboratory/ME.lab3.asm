.data
	name: .space 100
	hello: .asciiz "Hello "
	newline: .asciiz "\n"
	exc: .asciiz "!"
	filehandle: .asciiz "name.txt"
.text
	# Get name size and store to s0
	li $v0, 5
	syscall
	move $s0, $v0
	
	addi $s0, $s0, 1
	
	# Get name string store to name
	la $a0, name
	move $a1, $s0
	li $v0, 8
	syscall
	
	# Print newline
	la $a0, newline
	li $v0, 4
	syscall
	
	# Print hello
	la $a0, hello
	li $v0, 4
	syscall
		
	# Print string name
	la $a0, name
	li $v0, 4
	syscall
	
	# Print newline
	la $a0, exc
	li $v0, 4
	syscall
	
	# Open name.txt
	la $a0, filehandle
	li $a1, 1
	li $a2, 0
	li $v0, 13
	syscall
	
	# file description
	move $t0, $v0
	
	
	# Write to file
	move $a0, $t0
	la $a1, name
	move $a2, $s0
	subi $a2, $a2, 1
	li $v0, 15
	syscall
	
	# Close file
	move $a0, $t0
	li $v0, 16
	syscall

	# exit
	li $v0, 10
	syscall