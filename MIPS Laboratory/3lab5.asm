.text
main:
	li $a0, 3
	jal fact
	
	move $a0, $v0
	li $v0, 1
	syscall

fact:	# fact(n): n>=0, return n!
	#push to stack $a0, $ra
	subi $sp, $sp, 8
	sw $ra, 4($sp)
	sw $a0, 0($sp)

	#if n<2, return 1
	blt $a0, 2, return1 
	
	#else get fact(n-1) 
	sub $a0, $a0, 1
	jal fact
	
	#pop from stack: a0, $ra
	lw $ra, 4($sp)
	lw $a0, 0($sp)
	addi $sp, $sp, 8
	
	#return n*fact(n-1)
	mul $v0, $a0, $v0
	jr $ra 
		
return1: 
	li $v0, 1
	addu $sp, $sp, 8
	jr $ra
