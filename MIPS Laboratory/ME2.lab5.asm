.text
main:
	li $a0, 0
	li $a1, 1
	li $a3, 7
	
	subi $a3, $a3, 2
	li $t0, 1
	jal fibo
	
	move $a0, $a2
	li $v0, 1
	syscall
	
exit:
	li $v0, 10
	syscall
	
fibo:	# fact(n): n>=0, return n!
	bgt $t0, $a3, done
	addi $t0, $t0, 1
	addu $a2, $a1, $a0
	
	move $a0, $a1
	move $a1, $a2
	
	j fibo

done:
	jr $ra
