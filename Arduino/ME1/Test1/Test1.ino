/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 

// give it a name:
int vcc = 9;
int pin10 = 10;
int pin11 = 11;
int pin12 = 12;
int pin13 = 13;

int delay1 = 1000;


  

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(vcc, OUTPUT);
  pinMode(pin10, OUTPUT);
  pinMode(pin11, OUTPUT);  
  pinMode(pin12, OUTPUT);
  pinMode(pin13, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  digitalWrite(vcc, HIGH);
  digitalWrite(pin10, LOW);
 digitalWrite(pin11, LOW); 
digitalWrite(pin12, LOW); 
digitalWrite(pin13, LOW);
delay(delay1);
digitalWrite(pin10, HIGH);
 digitalWrite(pin11, LOW); 
digitalWrite(pin12, LOW); 
digitalWrite(pin13, LOW);
delay(delay1);
digitalWrite(pin10, LOW);
 digitalWrite(pin11, HIGH); 
digitalWrite(pin12, LOW); 
digitalWrite(pin13, LOW);
delay(delay1);
digitalWrite(pin10, HIGH);
 digitalWrite(pin11, HIGH); 
digitalWrite(pin12, LOW); 
digitalWrite(pin13, LOW);
delay(delay1);
digitalWrite(pin10, LOW);
 digitalWrite(pin11, LOW); 
digitalWrite(pin12, HIGH); 
digitalWrite(pin13, LOW);
delay(delay1);
digitalWrite(pin10, HIGH);
 digitalWrite(pin11, LOW); 
digitalWrite(pin12, HIGH); 
digitalWrite(pin13, LOW);
delay(delay1);
digitalWrite(pin10, LOW);
 digitalWrite(pin11, HIGH); 
digitalWrite(pin12, HIGH); 
digitalWrite(pin13, LOW);
delay(delay1);
digitalWrite(pin10, HIGH);
 digitalWrite(pin11, HIGH); 
digitalWrite(pin12, HIGH); 
digitalWrite(pin13, LOW);
delay(delay1);
digitalWrite(pin10, LOW);
 digitalWrite(pin11, LOW); 
digitalWrite(pin12, LOW); 
digitalWrite(pin13, HIGH);
delay(delay1);
digitalWrite(pin10, HIGH);
 digitalWrite(pin11, LOW); 
digitalWrite(pin12, LOW); 
digitalWrite(pin13, HIGH);
delay(delay1);
}

/*
digitalWrite(led, HIGH); 
 delay(400);               // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(100);      
   digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);               // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(100);      
   digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(400);               // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(100);      
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(300);               // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(100);    
digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);               // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);      
 }
//*/
