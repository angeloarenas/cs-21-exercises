#include "pitches.h"

int melody[] = {
  NOTE_C4, NOTE_G3,NOTE_G3, NOTE_A3, NOTE_G3,0, NOTE_B3, NOTE_C4};

// note durations: 4 = quarter note, 8 = eighth note, etc.:
int noteDurations[] = {
  4, 8, 8, 4,4,4,4,4 };

const int led1 = 2;
const int led2 = 3;
const int led3 = 4;
const int led4 = 5;
const int led5 = 6;

void setup() {
  pinMode(led1, HIGH);
  pinMode(led2, HIGH);
  pinMode(led3, HIGH);
  pinMode(led4, HIGH);
  pinMode(led5, HIGH);

  // iterate over the notes of the melody:
  for (int thisNote = 0; thisNote < 8; thisNote++) {
    int noteDuration = 1000/noteDurations[thisNote];

    // to calculate the note duration, take one second 
    // divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    tone(13, melody[thisNote],noteDuration);
    lightup(thisNote);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(13);
  }
}

void loop() {
}

void lightup(int x) {
  switch(x) {
    case 0:
      digitalWrite(led1, HIGH);
      
      digitalWrite(led2, LOW);
      digitalWrite(led3, LOW);
      digitalWrite(led4, LOW);
      digitalWrite(led5, LOW);
    break;
    case 1: 
      digitalWrite(led2, HIGH);
      
      digitalWrite(led1, LOW);
      digitalWrite(led3, LOW);
      digitalWrite(led4, LOW);
      digitalWrite(led5, LOW);
    break;
    case 2: case 7:
      digitalWrite(led3, HIGH);
      
      digitalWrite(led1, LOW);
      digitalWrite(led2, LOW);
      digitalWrite(led4, LOW);
      digitalWrite(led5, LOW);
    break;
    case 3: case 6:
      digitalWrite(led4, HIGH);
      
      digitalWrite(led1, LOW);
      digitalWrite(led2, LOW);
      digitalWrite(led3, LOW);
      digitalWrite(led5, LOW);
    break;
    case 4:
      digitalWrite(led5, HIGH);
      
      digitalWrite(led1, LOW);
      digitalWrite(led2, LOW);
      digitalWrite(led3, LOW);
      digitalWrite(led4, LOW);
    break;
  }
}
