
#include "TimerOne.h"

#define TESTMODE // continuously sequence thru the LED's 

#define DIM 5 // x/y dimension - 5x5 matrix
#define DIM1 (DIM-1)

typedef byte Frame[DIM];

#include "frames.h"

byte row = 0;
byte *curFrame;
int curFrameIdx;

int cols[DIM] = {12,11,10,9,8};

int rows[DIM] = {7,6,5,4,3};

Frame blankFrame =
{B00000,
 B01100,
 B00000,
 B00000,
 B00000};

// blank the screen
void clearLeds() {
  // blank screen
  curFrame = blankFrame;
}

// select a frame to display
// idx = 0 -> FRAMDECNT-1
void setFrame(int idx) {
  curFrame = Frames[idx];
}

byte bitMask = B00000011;
void display() {
  digitalWrite(rows[row], HIGH);  // Turn whole previous row off

  if (bitMask == B00010000) {
    bitMask = B00000011;  // light the right 2 columns (pins 9,8)
    // increment row and wrap if necessary
    if (++row == DIM) {
      row = 0;
    }
  }
  else if (bitMask == B00000011) {
    bitMask = B00001100;  // light the middle 2 columns (pins 11,10)
  }
  else { // bitMaskIdx == B00001100
    bitMask = B00010000;  // light the leftmost column (pin 12)
  }
  
  PORTB &= B11100000;
  PORTB |= curFrame[row] & bitMask;
  
  digitalWrite(rows[row], LOW); // Turn whole row on at once (for equal lighting times)
}


void setup() {
  int i;
  
  // sets the pins as output
  for (i = 0; i < DIM; i++) {
    pinMode(cols[i], OUTPUT);
    pinMode(rows[i], OUTPUT);
  }

  // set up cols and rows (set display to dark)
  for (i = 0; i < DIM; i++) {
    digitalWrite(cols[i], LOW);
  }

  for (i = 0; i < DIM; i++) {
    digitalWrite(rows[i], HIGH);
  }


  clearLeds();
  Timer1.initialize(1000);
  
  Timer1.attachInterrupt(display); // ISR

  curFrameIdx = -1;
                                                 
}

int time_blocks_start = 159;
int time_blocks_end = 179;
int time_ourheartsarelike_pause = 194;
int time_ourheartsarelike_start = 195;
int time_ourheartsarelike_end = 214;
int time_sparksfly_start = 215;
int time_whentheystrike_pause = 237;
int time_lightworld_start = 247;
int time_lightworld_end = 263;
int time_tuntenten_start = 264;
int time_tuntenten_end = 318;
int time_tuntenten2_start = 319;
int time_tuntenten2_end = 359;

int repetitions_blocks_counter = 0;
int repetitions_blocks = 7;
int repetitions_ourheartsarelike_counter = 0;
int repetitions_ourheartsarelike = 4;
int repetitions_lightworld_counter = 0;
int repetitions_lightworld = 2;
int repetitions_sparkworld_counter = 0;
int repetitions_sparkworld = 2;
int repetitions_tuntenten_counter = 0;
int repetitions_tuntenten = 3;
int repetitions_tuntenten2_counter = 0;
int repetitions_tuntenten2 = 7;

int heartbeat_delay = 1000;
void loop() {
  // increment the frame index and wrap if necessary
  if (++curFrameIdx == (FRAMECNT)) {
    curFrameIdx = 0;
  } else if (curFrameIdx == time_blocks_end && ++repetitions_blocks_counter < repetitions_blocks) {
    curFrameIdx = time_blocks_start;
  } else if (curFrameIdx == time_ourheartsarelike_pause) {
    if (repetitions_sparkworld_counter == 0)
      delay(800);
    else if (repetitions_sparkworld_counter == 1)
      delay(900);
  } else if (curFrameIdx == time_ourheartsarelike_end && ++repetitions_ourheartsarelike_counter < repetitions_ourheartsarelike) {
    if (repetitions_sparkworld_counter == 0)
      delay(100*repetitions_ourheartsarelike_counter);
    if (repetitions_sparkworld_counter == 1)
       delay(30*repetitions_ourheartsarelike_counter);
    curFrameIdx = time_ourheartsarelike_start;
  } else if (curFrameIdx == time_whentheystrike_pause) {
    delay(500);
  } else if (curFrameIdx == time_lightworld_end && ++repetitions_lightworld_counter < repetitions_lightworld) {
    curFrameIdx = time_lightworld_start;
    if (repetitions_sparkworld_counter == 1) {
        if (repetitions_lightworld_counter == 5) {
           curFrameIdx = 0;
           setFrame(curFrameIdx);
           delay(600);
           curFrameIdx = time_lightworld_end+1;
        }
        repetitions_lightworld = 6;
    }
  } else if (curFrameIdx == time_lightworld_end && ++repetitions_sparkworld_counter < repetitions_sparkworld) {
    curFrameIdx = time_ourheartsarelike_pause-1;
    repetitions_lightworld_counter = 0;
    repetitions_ourheartsarelike_counter = 0;
  } else if (curFrameIdx == time_tuntenten_end && ++repetitions_tuntenten_counter < repetitions_tuntenten) {
    delay(10);
    curFrameIdx = time_tuntenten_start;
  } else if (curFrameIdx == time_tuntenten2_end && ++repetitions_tuntenten2_counter < repetitions_tuntenten2) {
    curFrameIdx = time_tuntenten2_start;
  }
  
  if (curFrameIdx == 315 && repetitions_tuntenten_counter == 2)
    curFrameIdx = time_tuntenten_end+1;
  

  // select frame for display
 setFrame(curFrameIdx);
   // display();

  delay( 105); // wait time between frames in ms
}



