/* 
 Debounce
 
 Each time the input pin goes from LOW to HIGH (e.g. because of a push-button
 press), the output pin is toggled from LOW to HIGH or HIGH to LOW.  There's
 a minimum delay between toggles to debounce the circuit (i.e. to ignore
 noise).  
 
 The circuit:
 * LED attached from pin 13 to ground
 * pushbutton attached from pin 2 to +5V
 * 10K resistor attached from pin 2 to ground
 
 * Note: On most Arduino boards, there is already an LED on the board
 connected to pin 13, so you don't need any extra components for this example.
 
 
 created 21 November 2006
 by ANGELO ARENAS
 modified 30 Aug 2011
 by Limor Fried
 
This example code is in the public domain.
 
 http://www.arduino.cc/en/Tutorial/Debounce
 */

// constants won't change. They're used here to 
// set pin numbers:
const int buttonPin = 2;     // the number of the pushbutton pin
const int ledPin =  11;      // the number of the LED pin
//const int testPin = 11;
// Variables will change:
int ledState = HIGH;         // the current state of the output pin
int prevReading = LOW;
long prev = 0;
long curr = 0;
long debounceDelay = 100;    // the debounce time; increase if the output flickers
int COUNTER = 0;

const int vcc = 9;

const int pin10 = 7;
const int pin11 = 6;
const int pin12 = 5;
const int pin13 = 4;

void setup() {
  pinMode(buttonPin, INPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(pin10, OUTPUT);
  pinMode(pin11, OUTPUT);
  pinMode(pin12, OUTPUT);
  pinMode(pin13, OUTPUT);
  pinMode(vcc, OUTPUT);
 // pinMode(testPin, OUTPUT);
}

void loop() {
  digitalWrite(vcc, HIGH);
  // read the state of the switch into a local variable:
  int reading = digitalRead(buttonPin);
  curr = millis();
  if (reading == HIGH & (reading != prevReading) & (curr-prev) > debounceDelay) {
    ledState = !ledState;
    prev = curr;
    digitalWrite(ledPin, ledState);
    COUNTER++;
  } // check to see if you just pressed the button 
  
  switch (COUNTER) {
    case 0:
      digitalWrite(pin10, LOW);
      digitalWrite(pin11, LOW); 
      digitalWrite(pin12, LOW); 
      digitalWrite(pin13, LOW);
    break;
    case 1:
      digitalWrite(pin10, HIGH);
      digitalWrite(pin11, LOW); 
      digitalWrite(pin12, LOW); 
      digitalWrite(pin13, LOW);
    break;
    case 2:
        digitalWrite(pin10, LOW);
 digitalWrite(pin11, HIGH); 
digitalWrite(pin12, LOW); 
digitalWrite(pin13, LOW);
    break;
    case 3:
    digitalWrite(pin10, HIGH);
 digitalWrite(pin11, HIGH); 
digitalWrite(pin12, LOW); 
digitalWrite(pin13, LOW);
    
    break;
    case 4:
    digitalWrite(pin10, LOW);
 digitalWrite(pin11, LOW); 
digitalWrite(pin12, HIGH); 
digitalWrite(pin13, LOW);
    
    break;
    case 5:
    digitalWrite(pin10, HIGH);
 digitalWrite(pin11, LOW); 
digitalWrite(pin12, HIGH); 
digitalWrite(pin13, LOW);


    break;
    case 6:
    digitalWrite(pin10, LOW);
 digitalWrite(pin11, HIGH); 
digitalWrite(pin12, HIGH); 
digitalWrite(pin13, LOW);
    
    break;
    case 7:
    digitalWrite(pin10, HIGH);
 digitalWrite(pin11, HIGH); 
digitalWrite(pin12, HIGH); 
digitalWrite(pin13, LOW);
    
    break;
    case 8:
    
    digitalWrite(pin10, LOW);
 digitalWrite(pin11, LOW); 
digitalWrite(pin12, LOW); 
digitalWrite(pin13, HIGH);
    break;
    case 9:
    digitalWrite(pin10, HIGH);
 digitalWrite(pin11, LOW); 
digitalWrite(pin12, LOW); 
digitalWrite(pin13, HIGH);
    break;
    case 10:
    COUNTER = 0;
    break;
  }
  prevReading = reading;
  
  //digitalWrite(testPin, reading)
}
