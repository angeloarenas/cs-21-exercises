/* Blink without Delay
 
 Turns on and off a light emitting diode(LED) connected to a digital  
 pin, without using the delay() function.  This means that other code
 can run at the same time without being interrupted by the LED code.
 
 The circuit:
 * LED attached from pin 13 to ground.
 * Note: on most Arduinos, there is already an LED on the board
 that's attached to pin 13, so no hardware is needed for this example.
 
 
 created 2005
 by David A. Mellis
 modified 8 Feb 2010
 by Paul Stoffregen
 
 This example code is in the public domain.

 
 http://www.arduino.cc/en/Tutorial/BlinkWithoutDelay
 */

// constants won't change. Used here to 
// set pin numbers:
const int buttonPin = 2;

const int led9 =  8;      // the number of the LED pin
const int led10 =  10;      // the number of the LED pin
const int led11 =  11;      // the number of the LED pin
const int led12 =  12;      // the number of the LED pin
const int led13 =  13;      // the number of the LED pin

const int pin10 = 7;
const int pin11 = 6;
const int pin12 = 5;
const int pin13 = 4;

int vcc = 3;

// Variables will change:
int led9STATE = LOW;             // ledState used to set the LED
int led10STATE = LOW;             // ledState used to set the LED
int led11STATE = LOW;             // ledState used to set the LED
int led12STATE = LOW;             // ledState used to set the LED
int led13STATE = LOW;             // ledState used to set the LED

int COUNTER = 0;
int COUNTERB = 0;

int ledState = HIGH;         // the current state of the output pin
int prevReading = LOW;
long prev = 0;
long curr = 0;
long debounceDelay = 100;   

long previousMillis = 0;        // will store last time LED was updated

// the follow variables is a long because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
long interval = 100;           // interval at which to blink (milliseconds)

void setup() {
  // set the digital pin as output:
  pinMode(led9, OUTPUT);      
  pinMode(led10, OUTPUT);      
  pinMode(led11, OUTPUT);      
  pinMode(led12, OUTPUT);      
  pinMode(led13, OUTPUT);  

 pinMode(pin10, OUTPUT);
  pinMode(pin11, OUTPUT);
  pinMode(pin12, OUTPUT);
  pinMode(pin13, OUTPUT);
  pinMode(vcc, OUTPUT);  
}

void loop()
{
  // here is where you'd put code that needs to be running all the time.

  // check to see if it's time to blink the LED; that is, if the 
  // difference between the current time and last time you blinked 
  // the LED is bigger than the interval at which you want to 
  // blink the LED.
  // read the state of the switch into a local variable:
  digitalWrite(vcc, HIGH);
  
  int reading = digitalRead(buttonPin);
  curr = millis();
  if (reading == HIGH & (reading != prevReading) & (curr-prev) > debounceDelay) {
    ledState = !ledState;
    prev = curr;
      //digitalWrite(ledPin, ledState);
  } // check to see if you just pressed the button 
  prevReading = reading;
  
  
  if(ledState == HIGH) {
        unsigned long currentMillis = millis();
       
        if(currentMillis - previousMillis > interval) {
          // save the last time you blinked the LED 
         
          switch (COUNTER) {
            case 0:
              digitalWrite(led9, HIGH);
              digitalWrite(led10, LOW);
              digitalWrite(led11, LOW);
              digitalWrite(led12, LOW);
              digitalWrite(led13, LOW);
            break;
            case 1:
              digitalWrite(led9, LOW);
              digitalWrite(led10, HIGH);
              digitalWrite(led11, LOW);
              digitalWrite(led12, LOW);
              digitalWrite(led13, LOW);
            break;
            case 2:
              digitalWrite(led9, LOW);
              digitalWrite(led10, LOW);
              digitalWrite(led11, HIGH);
              digitalWrite(led12, LOW);
              digitalWrite(led13, LOW);
            break;
            case 3:
              digitalWrite(led9, LOW);
              digitalWrite(led10, LOW);
              digitalWrite(led11, LOW);
              digitalWrite(led12, HIGH);
              digitalWrite(led13, LOW);
              
            break;
            case 4:
              digitalWrite(led9, LOW);
              digitalWrite(led10, LOW);
              digitalWrite(led11, LOW);
              digitalWrite(led12, LOW);
              digitalWrite(led13, HIGH);
            break;
            case 5:
              COUNTER = -1;
            break;
          }
           COUNTER++;
          previousMillis = currentMillis;   
  }
  
  else if (ledState == LOW) {
        switch (COUNTER) {
                case 0:
                  digitalWrite(led9, HIGH);
                  digitalWrite(led10, LOW);
                  digitalWrite(led11, LOW);
                  digitalWrite(led12, LOW);
                  digitalWrite(led13, LOW);
                break;
                case 1:
                  digitalWrite(led9, LOW);
                  digitalWrite(led10, HIGH);
                  digitalWrite(led11, LOW);
                  digitalWrite(led12, LOW);
                  digitalWrite(led13, LOW);
                break;
                case 2:
                  digitalWrite(led9, LOW);
                  digitalWrite(led10, LOW);
                  digitalWrite(led11, HIGH);
                  digitalWrite(led12, LOW);
                  digitalWrite(led13, LOW);
                  COUNTERB++;
                break;
                case 3:
                  digitalWrite(led9, LOW);
                  digitalWrite(led10, LOW);
                  digitalWrite(led11, LOW);
                  digitalWrite(led12, HIGH);
                  digitalWrite(led13, LOW);
                  
                break;
                case 4:
                  digitalWrite(led9, LOW);
                  digitalWrite(led10, LOW);
                  digitalWrite(led11, LOW);
                  digitalWrite(led12, LOW);
                  digitalWrite(led13, HIGH);
                break;
                case 5:
                  COUNTER = 0;
                break;
              }
  }
  
  
  switch (COUNTERB) {
    case 0:
      digitalWrite(pin10, LOW);
      digitalWrite(pin11, LOW); 
      digitalWrite(pin12, LOW); 
      digitalWrite(pin13, LOW);
    break;
    case 1:
      digitalWrite(pin10, HIGH);
      digitalWrite(pin11, LOW); 
      digitalWrite(pin12, LOW); 
      digitalWrite(pin13, LOW);
    break;
    case 2:
        digitalWrite(pin10, LOW);
 digitalWrite(pin11, HIGH); 
digitalWrite(pin12, LOW); 
digitalWrite(pin13, LOW);
    break;
    case 3:
    digitalWrite(pin10, HIGH);
 digitalWrite(pin11, HIGH); 
digitalWrite(pin12, LOW); 
digitalWrite(pin13, LOW);
    
    break;
    case 4:
    digitalWrite(pin10, LOW);
 digitalWrite(pin11, LOW); 
digitalWrite(pin12, HIGH); 
digitalWrite(pin13, LOW);
    
    break;
    case 5:
    digitalWrite(pin10, HIGH);
 digitalWrite(pin11, LOW); 
digitalWrite(pin12, HIGH); 
digitalWrite(pin13, LOW);


    break;
    case 6:
    digitalWrite(pin10, LOW);
 digitalWrite(pin11, HIGH); 
digitalWrite(pin12, HIGH); 
digitalWrite(pin13, LOW);
    
    break;
    case 7:
    digitalWrite(pin10, HIGH);
 digitalWrite(pin11, HIGH); 
digitalWrite(pin12, HIGH); 
digitalWrite(pin13, LOW);
    
    break;
    case 8:
    
    digitalWrite(pin10, LOW);
 digitalWrite(pin11, LOW); 
digitalWrite(pin12, LOW); 
digitalWrite(pin13, HIGH);
    break;
    case 9:
    digitalWrite(pin10, HIGH);
 digitalWrite(pin11, LOW); 
digitalWrite(pin12, LOW); 
digitalWrite(pin13, HIGH);
    break;
  }
/*
    // if the LED is off turn it on and vice-versa:
    if (ledState == LOW)
      ledState = HIGH;
    else
      ledState = LOW;

    // set the LED with the ledState of the variable:
    digitalWrite(ledPin, ledState);
    */
  }
}

