#include "pitches.h"

int melody[] = {
  NOTE_C4, NOTE_A3,NOTE_B3, NOTE_A3, NOTE_G3,0, NOTE_B3, NOTE_C4};

// note durations: 4 = quarter note, 8 = eighth note, etc.:
int noteDurations[] = {
  4, 8, 8, 4,4,4,4,4 };

const int input1 = 2;
const int input2 = 3;
const int input3 = 4;

const int led8 = 8;
const int led9 = 9;
const int led10 = 10;

int isasakanila;

void setup() {
  pinMode(input1, INPUT);
  pinMode(input2, INPUT);
  pinMode(input3, INPUT);
  pinMode(led8, OUTPUT);
  pinMode(led9, OUTPUT);
  pinMode(led10, OUTPUT);
}

void loop() {
  
  int reading1 = digitalRead(input1);
    int reading2 = digitalRead(input2);
    int reading3 = digitalRead(input3);
  
  if (reading1 == LOW && reading2 == LOW && reading3 == LOW) {
    isasakanila = false;
    noTone(12);
  }
    
  if (reading1 == HIGH && isasakanila == false) {
    digitalWrite(led8, HIGH);
    
    isasakanila = true;
    sound(0);
  } else if (reading1 == LOW) {
    digitalWrite(led8, LOW);
  }
  if (reading2 == HIGH && isasakanila == false) {
    digitalWrite(led9, HIGH);
    
    isasakanila = true;
    sound(1);
  } else if (reading2 == LOW) {
    digitalWrite(led9, LOW);
  }
  if (reading3 == HIGH && isasakanila == false) {
    digitalWrite(led10, HIGH);
    isasakanila = true;
    sound(2);
  } else if (reading3 == LOW) {
    digitalWrite(led10, LOW);
  }
}

void sound(int x) {
  tone(12, melody[x], 100000000);
  
  switch(x) {
    case 1:
    break;
    case 2:
    break;
    case 3:
    break;
  }
}
