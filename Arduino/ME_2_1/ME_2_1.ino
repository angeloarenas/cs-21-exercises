/* 
 Debounce
 
 Each time the input pin goes from LOW to HIGH (e.g. because of a push-button
 press), the output pin is toggled from LOW to HIGH or HIGH to LOW.  There's
 a minimum delay between toggles to debounce the circuit (i.e. to ignore
 noise).  
 
 The circuit:
 * LED attached from pin 13 to ground
 * pushbutton attached from pin 2 to +5V
 * 10K resistor attached from pin 2 to ground
 
 * Note: On most Arduino boards, there is already an LED on the board
 connected to pin 13, so you don't need any extra components for this example.
 
 
 created 21 November 2006
 by ANGELO ARENAS
 modified 30 Aug 2011
 by Limor Fried
 
This example code is in the public domain.
 
 http://www.arduino.cc/en/Tutorial/Debounce
 */

// constants won't change. They're used here to 
// set pin numbers:
long previousMillis = 0;        // will store last time LED was updated

// the follow variables is a long because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
long interval = 1000;      
const int buttonPin = 2;     // the number of the pushbutton pin
const int ledPin =  11;      // the number of the LED pin
int brightness = 0;    // how bright the LED is
int fadeAmount = 5; 
// Variables will change:
int ledState = HIGH;         // the current state of the output pin
int ledState2 = LOW;         // the current state of the output pin

int prevReading = LOW;
long prev = 0;
long curr = 0;
int bright = 0;
int fade = 5;
long debounceDelay = 100;    // the debounce time; increase if the output flickers

void setup() {
  pinMode(buttonPin, INPUT);
  pinMode(ledPin, OUTPUT);

}

void loop() {
   // read the state of the switch into a local variable:
  int reading = digitalRead(buttonPin);
  curr = millis();
  if (reading == HIGH & (reading != prevReading) & (curr-prev) > debounceDelay) {
    ledState = !ledState;
    prev = curr;
    digitalWrite(ledPin, ledState);
  } // check to see if you just pressed the button 
  prevReading = reading;
  //digitalWrite(testPin, reading);
  
  if (ledState == HIGH){
    
        unsigned long currentMillis = millis();
     
      if(currentMillis - previousMillis > interval) {
        // save the last time you blinked the LED 
        previousMillis = currentMillis;   
    
        // if the LED is off turn it on and vice-versa:
        if (ledState2 == LOW)
          ledState2 = HIGH;
        else
          ledState2 = LOW;
    
        // set the LED with the ledState of the variable:
        digitalWrite(ledPin, ledState2);
      }
  }
  else if (ledState == LOW) {
            // set the brightness of pin 9:
        analogWrite(ledPin, brightness);    
      
        // change the brightness for next time through the loop:
        brightness = brightness + fadeAmount;
      
        // reverse the direction of the fading at the ends of the fade: 
        if (brightness == 0 || brightness == 255) {
          fadeAmount = -fadeAmount ; 
        }     
        // wait for 30 milliseconds to see the dimming effect    
        delay(30);         
  }
}

