# I hereby certify upon my honor that
# I worked on this code independently.
# I got assistance from: <Stack overflow, 
# MIPS Reference Data, SYSCALL functions available in MARS,
# Computer Organization and Design (Patterson, Hennessy)>
# Signed, <Angelo Arenas>, <2014-89285>
.data
	scalarproduct: .asciiz "Scalar Product: "
.text
main:
	#Input x to $s0
	li $v0, 5 
	syscall 
	move $s0, $v0
	
	#Input y to $s1
	li $v0, 5 
	syscall 
	move $s1, $v0
	
	#$s2 contains AND of $s0 and $s1
	and $s2, $s0, $s1
	
	#$s3 contains a copy of $s2 (not gonna use $s2 inside loop for easier checking)
	move $s3, $s2

	#Least significant bit of $s2 stored to $t0 (initially)	
	and $t0, $s2, 1
	
	#Execute loop
	jal xorloop
	
	#Print here
	la $a0, scalarproduct
	li $v0, 4
	syscall
	move $a0, $t0
	li $v0, 1
	syscall
	
exit: 
	li $v0, 10
	syscall

exitloop:
	#Return to next line at main
	jr $ra
	
xorloop:
	#Shift $s3 to the right by one bit (if 0 contents then exit)
	srl $s3, $s3, 1
	beq $s3, 0, exitloop
	
	#Store LSB of $s3 to $t1
	move $t1, $s3
	and $t1, $t1, 1
	
	#Store to $t0 the XOR of $t0 and $t1
	xor $t0, $t0, $t1
	
	#Loop back
	j xorloop
