# I hereby certify upon my honor that
# I worked on this code independently.
# I got assistance from: <Stack Overflow, 
# MIPS Reference Data, SYSCALL functions available in MARS,
# Computer Organization and Design (Patterson, Hennessy)>
# Signed, <Angelo Arenas>, <2014-89285>

.data
	input: .space 100
	palin_out: .asciiz "\nYes"
	notpalin_out: .asciiz "\nNo"
.text

# $s0 - string length
# $s1 - address string input
# $t0 - address string input copy 1 (left)
# $t1 - address string input copy 2 (right)
# $t2 - byte from string starting at leftmost char
# $t3 - byte from string starting at rightmost char
# $t4 - loop counter
main:
	#Input number of characters to read
	#Put in $s0 for later use
	li $v0, 5
	syscall
	move $s0, $v0
	addi $s0, $s0, 1
	
	#Input string, put to $s1
	la $a0, input
	move $a1, $s0
	li $v0, 8
	syscall
	la $s1, input
	
	#Make copies to $t0 and $t1 then adjust $t1 to last character
	move $t0, $s1
	move $t1, $s1
	addu $t1, $t1, $s0
	subi $t1, $t1, 2
	
	#Initiate counter then loop
	li $t4, 0
	jal palinloop
	
exit:
	li $v0, 10
	syscall
	
palinloop:
	# Load each character from string 
	# $t2 starts left, $t3 starts right
	lb $t2, 0($t0)
	lb $t3, 0($t1)
	# Point the address to next letter
	addi $t0, $t0, 1
	subi $t1, $t1, 1
	# Increment counter
	addi $t4, $t4, 1
	# If $t2 and $t3 not equal then exit
	bne $t2, $t3, notpalin
	# If counter is equal to string length
	beq $t4, $s0, palin
	j palinloop
	
palin:
	# Output Yes
	la $a0, palin_out
	li $v0, 4
	syscall
	jr $ra
	
notpalin:
	# Output No
	la $a0, notpalin_out
	li $v0, 4
	syscall
	jr $ra

