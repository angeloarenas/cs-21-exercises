# I hereby certify upon my honor that
# I worked on this code independently.
# I got assistance from: <Stack Overflow, 
# MIPS Reference Data, SYSCALL functions available in MARS,
# Computer Organization and Design (Patterson, Hennessy)>
# Signed, <Angelo Arenas>, <2014-89285>

# *Speed of computer may vary*
# File "morse.txt" must be in same directory as Mars.jar

.data
	filename: .asciiz "morse.txt"
	outbuffer: .space 101
.text
main:
	li $s0, 100	#Dot duration
	li $s1, 300	#Dash duration, space bet. letters
	li $s2, 500	#Space bet. words
	
	#Open file
	la $a0, filename
	li $a1, 0
	li $a2, 0
	li $v0, 13
	syscall
	#file descriptor stored at $t0
	move $t0, $v0
	#Read file
	move $a0, $t0
	la $a1, outbuffer
	li $a2, 100
	li $v0, 14
	syscall
	
	#Print string (FOR DEBUG)
	#la $a0, outbuffer
	#li $v0, 4
	#syscall
	
	#Load address of outbuffer to $t1
	la $t1, outbuffer
	
	j morseloop
	
	#close file
	move $a0, $t0
	li $v0, 16
	syscall
	
	j exit
morseloop:
	lb $t2, 0($t1)
	addi $t1, $t1, 1
	beq $t2, 46, exit #ASCII for . is 46
	beq $t2, 32, _wordspace
	# Divided into sections so it would be slightly faster
	blt $t2, 102, a_e
	blt $t2, 108, f_k
	blt $t2, 114, l_q
	blt $t2, 119, r_v
	blt $t2, 123, w_z
	
a_e:
	beq $t2, 97, _a
	beq $t2, 98, _b
	beq $t2, 99, _c
	beq $t2, 100, _d
	beq $t2, 101, _e	
f_k:
	beq $t2, 102, _f
	beq $t2, 103, _g
	beq $t2, 104, _h
	beq $t2, 105, _i
	beq $t2, 106, _j
	beq $t2, 107, _k
l_q: 
	beq $t2, 108, _l
	beq $t2, 109, _m
	beq $t2, 110, _n
	beq $t2, 111, _o
	beq $t2, 112, _p
	beq $t2, 113, _q
r_v:
	beq $t2, 114, _r
	beq $t2, 115, _s
	beq $t2, 116, _t
	beq $t2, 117, _u
	beq $t2, 118, _v
w_z:
	beq $t2, 119, _w
	beq $t2, 120, _x
	beq $t2, 121, _y
	beq $t2, 122, _z
_dot:
	li $a0, 67			
	move $a1, $s0			
	li $a2, 0			
	li $a3, 64			
	li $v0, 33		
	syscall
	jr $ra
_dash:
	li $a0, 67			
	move $a1, $s1		
	li $a2, 0			
	li $a3, 64			
	li $v0, 33			
	syscall
	jr $ra
_letterspace:
	move $a0, $s1
	li $v0, 32
	syscall
	jr $ra
_wordspace:
	move $a0, $s2
	li $v0, 32
	syscall
	j morseloop
_a:
	jal _dot
	jal _dash
	jal _letterspace
	j morseloop
_b:
	jal _dash
	jal _dot
	jal _dot
	jal _dot
	jal _letterspace
	j morseloop
_c:
	jal _dash
	jal _dot
	jal _dash
	jal _dot
	jal _letterspace
	j morseloop
_d:
	jal _dash
	jal _dot
	jal _dot
	jal _letterspace
	j morseloop
_e:
	jal _dot
	jal _letterspace
	j morseloop
_f:
	jal _dot
	jal _dot
	jal _dash
	jal _dot
	jal _letterspace
	j morseloop
_g:
	jal _dash
	jal _dash
	jal _dot
	jal _letterspace
	j morseloop
_h:
	jal _dot
	jal _dot
	jal _dot
	jal _dot
	jal _letterspace
	j morseloop
_i:
	jal _dot
	jal _dot
	jal _letterspace
	j morseloop
_j:
	jal _dot
	jal _dash
	jal _dash
	jal _dash
	jal _letterspace
	j morseloop
_k:
	jal _dash
	jal _dot
	jal _dash
	jal _letterspace
	j morseloop
_l:
	jal _dot
	jal _dash
	jal _dot
	jal _dot
	jal _letterspace
	j morseloop
_m:
	jal _dash
	jal _dash
	jal _letterspace
	j morseloop
_n:
	jal _dash
	jal _dot
	jal _letterspace
	j morseloop
_o:
	jal _dash
	jal _dash
	jal _dash
	jal _letterspace
	j morseloop
_p:
	jal _dot
	jal _dash
	jal _dash
	jal _dot
	jal _letterspace
	j morseloop
_q:
	jal _dash
	jal _dash
	jal _dot
	jal _dash
	jal _letterspace
	j morseloop
_r:
	jal _dot
	jal _dash
	jal _dot
	jal _letterspace
	j morseloop
_s:
	jal _dot
	jal _dot
	jal _dot
	jal _letterspace
	j morseloop
_t:
	jal _dash
	jal _letterspace
	j morseloop
_u:
	jal _dot
	jal _dot
	jal _dash
	jal _letterspace
	j morseloop
_v:
	jal _dot
	jal _dot
	jal _dot
	jal _dash
	jal _letterspace
	j morseloop
_w:
	jal _dot
	jal _dash
	jal _dash
	jal _letterspace
	j morseloop
_x:
	jal _dash
	jal _dot
	jal _dot
	jal _dash
	jal _letterspace
	j morseloop
_y:
	jal _dash
	jal _dot
	jal _dash
	jal _dash
	jal _letterspace
	j morseloop
_z:
	jal _dash
	jal _dash
	jal _dot
	jal _dot
	jal _letterspace
	j morseloop
exit:
	li $v0, 10
	syscall
